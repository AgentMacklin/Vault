# VAULT

A command line application that allows you to save your usernames and passwords in a SQLite database for later use. VAULT can also generate random passwords for you to use.
