"""

██╗   ██╗ █████╗ ██╗   ██╗██╗  ████████╗
██║   ██║██╔══██╗██║   ██║██║  ╚══██╔══╝
██║   ██║███████║██║   ██║██║     ██║
╚██╗ ██╔╝██╔══██║██║   ██║██║     ██║
 ╚████╔╝ ██║  ██║╚██████╔╝███████╗██║
  ╚═══╝  ╚═╝  ╚═╝ ╚═════╝ ╚══════╝╚═╝

Author: Austen LeBeau
VAULT is a command line application that allows a user to store passwords and usernames
in a SQLite database. It can also generate a random password for you.

"""

import click
import random
import string
import sqlite3

"""
TODO:
1. Create a column for username/email.
2. Figure out if there is a way to hide the output of passwords to the terminal with asterisks or something similar.
"""

# =========== #
#   Classes   #
# =========== #


class Database(object):

    def __init__(self):
        ''' The init function creates a connection to the database everytime an instance is created,
            and checks to see if a table already exists. If one doesn't, init creates it. '''
        self.conn = sqlite3.connect("password_safe.db")
        self.c = self.conn.cursor()

        # Creates a table if there isn't one already, otherwise does nothing.
        self.conn.execute("""CREATE TABLE IF NOT EXISTS
        passwords (name TEXT, username TEXT, password TEXT)""")

    def delete_data(self, name):
        ''' Deletes data. '''
        while True:
            prompt = click.prompt("Delete this entry? [y/n]", type=str)
            if prompt == 'y':
                self.c.execute(
                    "DELETE FROM passwords WHERE name=:name", (name,))
                break
            elif prompt == 'n':
                break
            elif prompt not in ('y', 'n'):
                click.echo("Please enter valid response.")

        self.conn.commit()
        self.conn.close()

    def update_password(self, name, close=False):
        ''' Updates password field. '''
        while True:
            randGen = click.prompt("Generate random password? [y/n]", type=str)
            if randGen.lower() == 'y':
                while True:
                    special = click.prompt(
                        "Add special characters? [y/n]", type=str)
                    if special.lower() == 'y':
                        length = click.prompt("Length", type=int)
                        password = generate_password(length, special=True)
                        break
                    elif special.lower() == 'n':
                        length = click.prompt("Length", type=int)
                        password = generate_password(length)
                        break
                    else:
                        click.echo("Please enter a valid response.")
                break

            elif randGen.lower() == 'n':
                password = click.prompt(
                    "Password", hide_input=True,
                    confirmation_prompt=True)
                break
            elif randGen not in ('y', 'n'):
                click.echo("Please enter a valid response.")

        self.c.execute('''UPDATE passwords
            SET password=:password
            WHERE name=:name''', (password, name))

        # This prevents closing the connection too early if this method is called by the insert_init
        # method, but can close it otherwise if it is called directly by the user.
        self.conn.commit()
        if close:
            self.conn.close()
        else:
            pass

    def update_username(self, name, close=False):
        ''' Updates username field. '''
        username = click.prompt("Enter new username/email", type=str)
        self.c.execute('''UPDATE passwords
            SET username=:username
            WHERE name=:name''', (username, name))

        self.conn.commit()
        if close:
            self.conn.close()
        else:
            pass

    def update_name(self, name, close=False):
        ''' Updates name of a database entry. '''
        newId = click.prompt("Enter new name", type=str)
        if " " in newId:
            newId = newId.replace(" ", "_")
        self.c.execute('''UPDATE passwords
            SET name=:newId
            WHERE name=:name''', (newId, name))

        self.conn.commit()
        if close:
            self.conn.close()
        else:
            pass

    def update_preexisting(self, name):
        entry = self.get_data(read=name)
        while True:
            rand = click.prompt("Generate a random password? [y/n]")
            if rand == 'y':
                while True:
                    spec = click.prompt("Use special characters? [y/n]")
                    if spec == 'y':
                        length = click.prompt("Length", type=int)
                        newPass = generate_password(length, special=True)
                        break
                    elif spec == 'n':
                        length = click.prompt("Length", type=int)
                        newPass = generate_password(length)
                        break
                    elif spec not in ('y', 'n'):
                        click.echo("Please enter a valid response.")

                break

            elif rand == 'n':
                newPass = click.prompt("Enter password", default=entry[2])
                break

            elif rand not in ('y', 'n'):
                click.echo("Please enter a valid response.")

        newUser = click.prompt("Enter username", default=entry[1])
        self.c.execute('''UPDATE passwords
            SET username=:newUser,
            password=:newPass
            WHERE name=:name''', (newUser, newPass, name))

    def insert_init(self, randPass=False, special=False):
        ''' Inserts data. Called by click init. '''
        name = click.prompt("Name")
        if ' ' in name:
            name = name.replace(' ', '_')

        if self.check_existence(name):
            click.echo(
                'Name already exists, would you like to edit existing database?')
            while True:
                prompt = click.prompt("Enter [y/n]", type=str)
                if prompt == 'y':
                    self.update_preexisting(name)
                    break
                elif prompt == 'n':
                    break
                elif prompt not in ('y', 'n'):
                    click.echo("Please enter a valid response.")

        else:
            username = click.prompt("Username/Email", type=str)

            if not randPass:
                password = click.prompt(
                    "Password", hide_input=True, confirmation_prompt=True)

            elif randPass:
                length = click.prompt("Length", type=int)
                if special:
                    password = generate_password(length, special)
                elif not special:
                    password = generate_password(length)

            self.c.execute(
                "INSERT INTO passwords VALUES (:name, :username, :password)",
                (name, username, password,))

        self.conn.commit()
        self.conn.close()

    def get_data(self, echo=False, **kwargs):
        ''' Retrieve data. '''
        for key, value in kwargs.items():
            if key == 'read' and value == 'all':
                self.c.execute("SELECT name FROM passwords")
                names = [tup[0] for tup in self.c.fetchall()]
                names.sort()
                for name in names:
                    click.echo(name)
            elif key == 'read' and value != 'all' and echo:
                self.c.execute(
                    "SELECT * FROM passwords WHERE name=:value", (value,))
                data = self.c.fetchone()
                click.echo("Username: {}\nPassword: {}".format(
                    data[1], data[2]))
            elif key == 'read' and value != 'all' and not echo:
                self.c.execute(
                    "SELECT * FROM passwords WHERE name=:value", (value,))
                data = self.c.fetchone()
                return data

    def check_existence(self, name):
        ''' Checks to make sure password is not already saved. '''
        self.c.execute("SELECT name FROM passwords")
        names = [tup[0] for tup in self.c.fetchall()]
        if name in names:
            return True
        else:
            return False


# ========= #
# Functions #
# ========= #

def generate_password(length, special=False):
    ''' Generates password. '''
    random.seed()
    upper = string.ascii_uppercase
    lower = string.ascii_lowercase
    digits = string.digits
    misc = '@#$%_!'
    password = ''

    for i in range(length):
        if special:
            bankSelect = random.randint(1, 4)
        elif not special:
            bankSelect = random.randint(1, 3)

        if bankSelect == 1:
            password += upper[random.randint(0, len(upper) - 1)]
        elif bankSelect == 2:
            password += lower[random.randint(0, len(lower) - 1)]
        elif bankSelect == 3:
            password += digits[random.randint(0, len(digits) - 1)]
        elif bankSelect == 4:
            password += misc[random.randint(0, len(misc) - 1)]

    return password


# =============== #
# Click Functions #
# =============== #

@click.group()
def cli():
    pass


@cli.command()
@click.option("-l", default=12, help='Length of password. Default is 12 characters.')
@click.option("-s", is_flag=True, help='Add special characters. Default is false.')
def gen(l, s):
    ''' Prints a random password to the screen. '''
    if s:
        password = generate_password(l, s)
    else:
        password = generate_password(l)

    click.echo(password)


@cli.command()
@click.option("-r", is_flag=True, help='Option to generate a random password. Default is false.')
@click.option("-s", is_flag=True, help='Create a password with special characters. Default is false.')
def init(r, s):
    """ Create a new password safe. """
    db = Database()
    if r:
        if s:
            db.insert_init(r, s)
        elif not s:
            db.insert_init(r)
    else:
        db.insert_init()


@cli.command()
@click.option("-a", is_flag=True, help="Display the names of all saved database entries.")
@click.option("-g", help="Retrieve username and password.")
def read(a, g):
    ''' Read or change existing data. '''
    db = Database()
    if a:
        db.get_data(read='all', echo=True)
    elif g:
        db.get_data(read=g, echo=True)


@cli.command()
@click.option("-d", help="Delete a database entry. Provide the associated name.")
@click.option("-p", help="Update a password. Provide the associated name.")
@click.option("-u", help="Update a username. Provide the associated name.")
@click.option("-n", help="Update the name of a database entry. Provide the associated name.")
def update(d, p, u, n):
    ''' Updates fields in the database. '''
    db = Database()
    if d:
        db.delete_data(d)
    elif p:
        db.update_password(p, close=True)
    elif u:
        db.update_username(u, close=True)
    elif n:
        db.update_name(n, close=True)
